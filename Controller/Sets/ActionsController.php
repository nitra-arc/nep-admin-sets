<?php

namespace Nitra\SetsBundle\Controller\Sets;

use Admingenerated\NitraSetsBundle\BaseSetsController\ActionsController as BaseActionsController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ActionsController extends BaseActionsController
{
    /**
     * @Route("/action_sets_autocomplete", name="Nitra_SetsBundle_Sets_autocomplete")
     * 
     * @param Request $request
     * 
     * @return JsonResponse
     */
    public function actionManagementAutocompleteProductAction(Request $request)
    {
        $class      = $request->query->get('class');
        $prototype  = $request->query->get('prototype');

        if ($id = $request->query->get('id')) {
            $obj = $this->getDocumentManager()->find($class, $id);
            return $this->formatResult(array(
                $obj->getId() => $obj,
            ), $prototype);
        }

        $cm = $this->getDocumentManager()->getClassMetadata($class);
        $qb = $this->getDocumentManager()->createQueryBuilder($class)
            ->limit(20)
            ->sort(array(
                'path'      => 'asc',
                'sortOrder' => 'asc',
                'name'      => 'asc',
            ));
        if ($cm->hasField('isActive')) {
            $qb->field('isActive')->equals(true);
        }

        $this->appendTermToQb($qb, $request->query->get('term'), $request->query->get('searchBy'));

        return $this->formatResult($qb->getQuery()->execute(), $prototype);
    }

    /**
     * @param \Doctrine\ODM\MongoDB\Query\Builder $qb
     * @param string $term
     * @param string $prototype
     */
    protected function appendTermToQb($qb, $term, $prototype)
    {
        foreach (explode(' ', $term) as $word) {
            if (trim($word)) {
                $Word = preg_quote($word);
                $qb ->addAnd($qb->expr()
                    ->field($prototype)->equals(new \MongoRegex("/$Word/i"))
                );
            }
        }
    }

    /**
     * formatting results
     * 
     * @param array $results
     * @param string $prototype
     * 
     * @return array
     */
    protected function formatResult($results, $prototype)
    {
        $result = array();
        foreach ($results as $key => $item) {
            $result[] = array(
                'value' => $key,
                'label' => $item->{'get' . $prototype}(),
            );
        }

        return new JsonResponse($result);
    }
}