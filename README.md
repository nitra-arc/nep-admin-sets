# SetsBundle

## Описание

данный бандл предназначен для:

* **создания и управления сетов**
 * Сет - набор товаров состоящий из одного основного и одного или нескольких не основных которые можно приобрести по какому-то выгодному предложению например со скидкой

## Подключение

* **composer.json**

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-admin-setsbundle": "dev-master",
        ...
    }
    ...
}
```

* **app/AppKernel.php**

```php
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\SetsBundle\NitraSetsBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

* **routing.yml**

```yml
NitraSetsBundle:
    resource:    "@NitraSetsBundle/Resources/config/routing.yml"
    prefix:      /{_locale}/
    defaults:
        _locale: ru
    requirements:
        _locale: en|ru
```

* **menu.yml**

```yml
sets:
    translateDomain: 'menu'
    route: Nitra_SetsBundle_Sets_list
```