<?php

namespace Nitra\SetsBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document
 */
class Sets
{
    use \Nitra\StoreBundle\Traits\LocaleDocument;

    /**
     * @ODM\Id(strategy="AUTO")
     */
    private $id;

    /**
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     * @Gedmo\Translatable
     */
    private $name;

    /**
     * @ODM\EmbedMany(targetDocument="SetEntry")
     */
    private $setEntries;

    /**
     * @ODM\Date
     */
    private $dateFrom;

    /**
     * @ODM\Date
     */
    private $dateTo;

    public function __construct()
    {
        $this->setEntries = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add setEntries
     *
     * @param Nitra\SetsBundle\Document\SetEntry $setEntries
     */
    public function addSetEntrie(\Nitra\SetsBundle\Document\SetEntry $setEntries)
    {
        $this->setEntries[] = $setEntries;
    }

    /**
     * Set setEntries
     *
     * @param Nitra\SetsBundle\Document\SetEntry $setEntries
     */
    public function setSetEntries($setEntries)
    {
        $this->setEntries = $setEntries;
    }

    /**
     * Remove setEntries
     *
     * @param <variableType$setEntries
     */
    public function removeSetEntrie(\Nitra\SetsBundle\Document\SetEntry $setEntries)
    {
        $this->setEntries->removeElement($setEntries);
    }

    /**
     * Get setEntries
     *
     * @return Doctrine\Common\Collections\Collection $setEntries
     */
    public function getSetEntries()
    {
        return $this->setEntries;
    }

    /**
     * Set dateFrom
     *
     * @param date $dateFrom
     * @return self
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * Get dateFrom
     *
     * @return date $dateFrom
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * Set dateTo
     *
     * @param date $dateTo
     * @return self
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;
        return $this;
    }

    /**
     * Get dateTo
     *
     * @return date $dateTo
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }
}