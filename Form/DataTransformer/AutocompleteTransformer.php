<?php

namespace Nitra\SetsBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Doctrine\ODM\MongoDB\DocumentManager;

class AutocompleteTransformer implements DataTransformerInterface
{
    /** @var \Doctrine\ODM\MongoDB\DocumentManager */
    protected $dm;

    /** @var string */
    protected $class;

    /** @var string */
    protected $prototype;

    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     */
    public function __construct(DocumentManager $dm, $class, $prototype = 'name')
    {
        $this->dm           = $dm;
        $this->class        = $class;
        $this->prototype    = $prototype;
    }

    /**
     * to object
     * @param string $value
     * @return object|null
     */
    public function reverseTransform($value)
    {
        return $this->dm->find($this->class, $value);
    }

    /**
     * to string
     * @param \Nitra\ProductBundle\Document\Product $value
     * @return string
     */
    public function transform($value)
    {
        if (!$value) {
            return null;
        }

        return json_encode(array(
            'value' => $value->getId(),
            'text'  => $value->{'get' . ucfirst($this->prototype)}(),
        ));
    }
}