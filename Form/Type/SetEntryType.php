<?php

namespace Nitra\SetsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\DependencyInjection\Container;
use Nitra\SetsBundle\Form\DataTransformer\AutocompleteTransformer;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

class SetEntryType extends AbstractType
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected $container;

    /** @var \Doctrine\ODM\MongoDB\DocumentManager */
    protected $dm;

    /**
     * @param \Symfony\Component\DependencyInjection\Container $container
     */
    public function __construct(Container $container)
    {
        $this->container    = $container;
        $this->dm           = $container->get('doctrine_mongodb.odm.document_manager');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type', 'choice', array(
            'label'         => 'sets.fields.setEntries.fields.type.label',
            'help'          => 'sets.fields.setEntries.fields.type.help',
            'empty_value'   => '',
            'choices'       => array(
                'category'      => 'sets.fields.setEntries.fields.type.choices.category',
                'product'       => 'sets.fields.setEntries.fields.type.choices.product',
            ),
        ));

        $builder->add('product', 'genemu_jqueryselect2_hidden', array(
            'configs'       => array(
                'query'         => 'function(query) {
                    functionAutocompleteQuery(query, "NitraProductBundle:Product", "fullNameForSearch");
                }',
                'initSelection' => 'function(element, callback) {
                    functionAutocompleteInitSelection(element, callback, "NitraProductBundle:Product", "fullNameForSearch");
                }',
            ),
            'label'         => 'sets.fields.setEntries.fields.product.label',
            'help'          => 'sets.fields.setEntries.fields.product.help',
            'required'      => false,
        ));

        $builder->add('category', 'genemu_jqueryselect2_hidden', array(
            'configs'       => array(
                'query'         => 'function(query) {
                    functionAutocompleteQuery(query, "NitraProductBundle:Category", "treeName", "name");
                }',
                'initSelection' => 'function(element, callback) {
                    functionAutocompleteInitSelection(element, callback, "NitraProductBundle:Category", "treeName");
                }',
            ),
            'label'         => 'sets.fields.setEntries.fields.category.label',
            'help'          => 'sets.fields.setEntries.fields.category.help',
            'required'      => false,
        ));

        $builder->add('main', 'choice', array(
            'label'         => 'sets.fields.setEntries.fields.main.label',
            'help'          => 'sets.fields.setEntries.fields.main.help',
            'choices'       => array(
                false           => 'sets.fields.setEntries.fields.main.choices.no',
                true            => 'sets.fields.setEntries.fields.main.choices.yes',
            ),
        ));
        $builder->add('discount', 'text', array(
            'label'         => 'sets.fields.setEntries.fields.discount.label',
            'help'          => 'sets.fields.setEntries.fields.discount.help',
        ));

        $builder->get('product')->addModelTransformer(new AutocompleteTransformer($this->dm, 'NitraProductBundle:Product', 'fullNameForSearch'));

        $builder->get('category')->addModelTransformer(new AutocompleteTransformer($this->dm, 'NitraProductBundle:Category'));

        $builder->addEventListener(FormEvents::SUBMIT, array($this, 'onSubmit'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'            => 'Nitra\SetsBundle\Document\SetEntry',
            'translation_domain'    => 'NitraSetsBundle',
            'error_bubbling'        => false,
        ));
    }

    public function getName()
    {
        return 'set_entry';
    }

    /**
     * validate entry
     * 
     * @param FormEvent $event
     */
    public function onSubmit(FormEvent $event)
    {
        // get form from event
        $form = $event->getForm();
        // if category and not main
        if ($form->get('type')->getData() == 'category' && !$form->get('main')->getData()) {
            $form->addError(new FormError("setEntry.categoryOnlyMain"));
        }

        // if not category and not product
        if (!$form->get('category')->getData() && !$form->get('product')->getData()) {
            $form->addError(new FormError("setEntry.categoryOrProductMustBeChanged"));
        }
    }
}